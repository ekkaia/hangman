export class Azerty {
	keyboard;

	constructor() {
		this.keyboard = [
			[
				new Key("a", KeyType.key),
				new Key("z", KeyType.key),
				new Key("e", KeyType.key),
				new Key("r", KeyType.key),
				new Key("t", KeyType.key),
				new Key("y", KeyType.key),
				new Key("u", KeyType.key),
				new Key("i", KeyType.key),
				new Key("o", KeyType.key),
				new Key("p", KeyType.key),
			], [
				new Key("q", KeyType.key),
				new Key("s", KeyType.key),
				new Key("d", KeyType.key),
				new Key("f", KeyType.key),
				new Key("g", KeyType.key),
				new Key("h", KeyType.key),
				new Key("j", KeyType.key),
				new Key("k", KeyType.key),
				new Key("l", KeyType.key),
				new Key("m", KeyType.key),
			], [
				new Key("w", KeyType.key),
				new Key("x", KeyType.key),
				new Key("c", KeyType.key),
				new Key("v", KeyType.key),
				new Key("b", KeyType.key),
				new Key("n", KeyType.key),
			]
		];
	}

	addSpecialKeys() {
		const stat = new Key("📊", KeyType.stat);
		stat.color = "bg-blue-gray-500";
		stat.hover = "hover:bg-blue-gray-400";
		stat.alt = "Statistiques";

		const params = new Key("⚙️", KeyType.params);
		params.color = "bg-cool-gray-500";
		params.hover = "hover:bg-cool-gray-400";
		params.alt = "Paramètres";

		const oracle = new Key("🔮", KeyType.oracle);
		oracle.color = "bg-violet-500";
		oracle.hover = "hover:bg-violet-400";
		oracle.alt = "Indice sur le thème";

		const magicWand = new Key("🪄", KeyType.magicWand);
		magicWand.color = "bg-indigo-500";
		magicWand.hover = "hover:bg-indigo-400";
		magicWand.alt = "Élimine 3 lettres";

		this.keyboard[this.keyboard.length - 1].unshift(stat, params);
		this.keyboard[this.keyboard.length - 1].push(oracle, magicWand);
	}

	get keyboard() {
		return this.keyboard;
	}
}

class Key {
	letter;
	color;
	hover;
	isDisabled;
	keyType;
	alt;

	constructor(letter, keyType) {
		this.letter = letter;
		this.keyType = keyType;
		this.color = "bg-true-gray-700";
		this.hover = "hover:bg-true-gray-600";
		this.isDisabled = false;
		this.alt = null;
	}

	/**
	 * @param {string} color
	 */
	set color(color) {
		if (this.isSpecialKey) {
			this.color = color;
		}
	}

	/**
	 * @param {string} hover
	 */
	set hover(hover) {
		if (this.isSpecialKey) {
			this.hover = hover;
		}
	}

	/**
	 * @param {string} alt
	 */
	set alt(alt) {
		if (this.isSpecialKey) {
			this.alt = alt;
		}
	}

	/**
	 * @param {boolean} disabled
	 */
	set isDisabled(disabled) {
		this.isDisabled = disabled;
	}
}

export class KeyType {
	static key = Symbol("key");
	static magicWand = Symbol("magicWand");
	static stat = Symbol("stat");
	static params = Symbol("params");
	static oracle = Symbol("oracle");
}
